package test.david.holamundo2021c1d.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UsuariosModelo {
    /**
     * Establecer comunicacion con SQLite para operar sobre
     * la tabla usuarios
     */

    private MainDBHelper dbHelper;

    public UsuariosModelo(Context context){
        this.dbHelper = new MainDBHelper(context);
    }

    /**
     * Funcion que me permite crear un nuevo usuario en la tabla
     */
    public boolean crearUsuario(String nombre, String password){
        // Construir objeto que se almacenara en la tabla
        ContentValues usuario = new ContentValues();

        // Definir cada atributo
        usuario.put(MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO, nombre);
        usuario.put(MainDBContract.TablaUsuarios.COLUMNA_PASSWORD, password);

        // Abrir comunicacion con la BD
        SQLiteDatabase bdCon = this.dbHelper.getWritableDatabase();

        // Insertar la fila
        bdCon.insert(MainDBContract.TablaUsuarios.NOMBRE_TABLA, null, usuario);

        // Cerrar la comunicacion
        bdCon.close();

        return true;
    }

    public ContentValues objeterUsuarioPorNombre(String nombre){
        // Abrir comunicacion con la BD
        SQLiteDatabase bdCon = this.dbHelper.getReadableDatabase();

        // SELECT * FROM usuarios WHERE nombreUsuario = 'lewis' LIMIT 1

        // Projection
        String[] projection = {
                MainDBContract.TablaUsuarios._ID,
                MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO,
                MainDBContract.TablaUsuarios.COLUMNA_PASSWORD
        };

        // Clausulas o condiciones
        String condiciones = MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO + " = ? LIMIT 1";

        String[] valoresCondiciones = { nombre };

        // Hacer la consulta a la BD
        Cursor resultadoPrevio = bdCon.query(
            MainDBContract.TablaUsuarios.NOMBRE_TABLA,
            projection,
            condiciones,
            valoresCondiciones,
            null, null, null
        );

        // Recorrer la respuesta para extraer los datos
        if(resultadoPrevio.moveToFirst()){
            // Si se mueve al primero, existe un resultado que coincide
            String nombre_bd = resultadoPrevio.getString(resultadoPrevio.getColumnIndex(MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO));
            String password_bd = resultadoPrevio.getString(resultadoPrevio.getColumnIndex(MainDBContract.TablaUsuarios.COLUMNA_PASSWORD));

            // Crear el objeto a devolver
            ContentValues usuario_bd = new ContentValues();

            // Definir cada atributo
            usuario_bd.put(MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO, nombre_bd);
            usuario_bd.put(MainDBContract.TablaUsuarios.COLUMNA_PASSWORD, password_bd);

            return usuario_bd;
        }

        // No existen datos
        return null;
    }
}
