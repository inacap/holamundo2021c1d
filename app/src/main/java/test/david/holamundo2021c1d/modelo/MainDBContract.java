package test.david.holamundo2021c1d.modelo;

import android.provider.BaseColumns;

public class MainDBContract {
    private MainDBContract(){}

    public static final String NOMBRE_BD = "MainDBC1D.db";
    public static final int VERSION_BD = 1;

    public static class TablaUsuarios implements BaseColumns {
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COLUMNA_NOMBREUSUARIO = "nombreUsuario";
        public static final String COLUMNA_PASSWORD = "password";
    }
}
