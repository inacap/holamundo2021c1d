package test.david.holamundo2021c1d.modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MainDBHelper extends SQLiteOpenHelper {

    private final String crearTablaUsuarios =
            "CREATE TABLE " + MainDBContract.TablaUsuarios.NOMBRE_TABLA + " (" +
                    MainDBContract.TablaUsuarios._ID + " INTEGER PRIMARY KEY, " +
                    MainDBContract.TablaUsuarios.COLUMNA_NOMBREUSUARIO + " TEXT, " +
                    MainDBContract.TablaUsuarios.COLUMNA_PASSWORD + " TEXT)";

    public MainDBHelper(Context ctx){
        super(ctx, MainDBContract.NOMBRE_BD, null, MainDBContract.VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Crear la(s) tabla(s)
        sqLiteDatabase.execSQL(this.crearTablaUsuarios);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO: Programar la actualizacion de la BD
    }
}
