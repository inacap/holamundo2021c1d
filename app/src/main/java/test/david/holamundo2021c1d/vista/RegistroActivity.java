package test.david.holamundo2021c1d.vista;

import androidx.appcompat.app.AppCompatActivity;
import test.david.holamundo2021c1d.R;
import test.david.holamundo2021c1d.controlador.UsuariosControlador;
import test.david.holamundo2021c1d.modelo.UsuariosModelo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        EditText etNombreUsuario = findViewById(R.id.etNombreUsuario);
        EditText etPassword1 = findViewById(R.id.etPassword1);
        EditText etPassword2 = findViewById(R.id.etPassword2);
        Button btRegistrarme = findViewById(R.id.btRegistrarme);

        btRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPassword1.getText().toString();
                String password2 = etPassword2.getText().toString();

                // Llamar a la capa controlador
                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());

                // crear el usuario
                String guardar = capaControlador.crearUsuario(nombreUsuario, password, password2);

                switch (guardar){
                    case "OK":
                        Toast.makeText(getApplicationContext(), "Usuario creado", Toast.LENGTH_LONG).show();
                        break;

                    case "ERR_NO_COINCICEN":
                        etPassword2.setError("Las contraseñas no coinciden");
                        break;

                    case "ERR_LARGO_CARACTERES":
                        etPassword1.setError("Debe tener 8 caracteres mínimo");
                        break;
                }
            }
        });

    }
}