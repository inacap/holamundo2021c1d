package test.david.holamundo2021c1d.vista;

import androidx.appcompat.app.AppCompatActivity;
import test.david.holamundo2021c1d.R;
import test.david.holamundo2021c1d.controlador.UsuariosControlador;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Consultar si alguien ya habia iniciado sesion
        // Abir archivo
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
        // obtener dato
        boolean inicioSesion = sesion.getBoolean("inicioSesion", false);

        if(inicioSesion){
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        //TextView tvBienvenida = findViewById(R.id.tvHolaMundo);
        //tvBienvenida.setText("Esto es desde el codigo.");

        EditText etNombreUsuario = findViewById(R.id.etUsername);
        EditText etPassword = findViewById(R.id.etPassword);

        TextView tvError = findViewById(R.id.tvError);
        TextView tvRegistrar = findViewById(R.id.tvRegistrar);

        // Reaccionar al click sobre "Crear un cuenta"
        tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Abrir una segunda ventana (Activity) para que la persona se registre
                Intent segundaActivity = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(segundaActivity);
            }
        });

        // Llamar al boton de la UI
        Button btnIngresar = findViewById(R.id.btnIngresar);

        // Definir un Listener para el evento click
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvError.setText("");

                // Reaccionar al click en el boton
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPassword.getText().toString();

                // Validar los datos
                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());

                boolean inicioSesion = capaControlador.procesarInicioSesion(nombreUsuario, password);
                if(inicioSesion){
                    // La persona demostro tener las credenciales correctas

                    // almacenar el inicio de sesion
                    // Abrir el archivo con los datos
                    SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                    // Poner el archivo en modo escritura
                    SharedPreferences.Editor editorSesion = sesion.edit();

                    // Escribir el dato a almacenar
                    editorSesion.putBoolean("inicioSesion", true);
                    editorSesion.putString("nombreUsuario", nombreUsuario);

                    // guardar y cerrar el archivo
                    editorSesion.commit();

                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Error en el inicio de sesion", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}