package test.david.holamundo2021c1d.vista;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import test.david.holamundo2021c1d.R;
import test.david.holamundo2021c1d.vista.fragmentos.BienvenidaFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity implements BienvenidaFragment.OnFragmentInteractionListener {

    private int contador;
    private BienvenidaFragment fragmento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button btnLogout = findViewById(R.id.btnLogout);
        TextView tvMensaje = findViewById(R.id.tvMensajeBienvenida);

        // Consultar si alguien ya habia iniciado sesion
        // Abir archivo
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
        // obtener dato
        String nombreUsuario = sesion.getString("nombreUsuario", "");
        tvMensaje.setText("Bienvenido a casa, " + nombreUsuario);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Abrir el archivo con los datos
                SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                // Poner el archivo en modo escritura
                SharedPreferences.Editor editorSesion = sesion.edit();

                // Escribir el dato a almacenar
                editorSesion.putBoolean("inicioSesion", false);
                editorSesion.putString("nombreUsuario", null);

                // guardar y cerrar el archivo
                editorSesion.commit();

                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);

                Toast.makeText(getApplicationContext(), "Hasta pronto!", Toast.LENGTH_SHORT).show();
            }
        });


        // Mostrar el fragment de bienvenida
        // Instanciar el administrador
        FragmentManager administradorFragmentos = getSupportFragmentManager();

        // Instanciar el fragmento a mostrar
        fragmento = new BienvenidaFragment();

        // Asignar el fragment a nuestro framelayout
        administradorFragmentos.beginTransaction().replace(R.id.frmLytPrincipal, fragmento).commit();

        contador = 0;
        Button btnMensaje = findViewById(R.id.btnMensaje);
        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Llamar al evento dentro del fragment
                contador++;
                fragmento.recibirMensaje("Ha hecho click " + contador + " veces");
            }
        });
    }

    @Override
    public void OnFragmentInteraction(String nombreEvento) {
        switch (nombreEvento){
            case "CLICK_TEXTVIEW":
                contador = 0;
                fragmento.recibirMensaje("Ha hecho click " + contador + " veces");
                break;
            case "CLICK_BUTTON":
                Toast.makeText(getApplicationContext(), "Hola", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}