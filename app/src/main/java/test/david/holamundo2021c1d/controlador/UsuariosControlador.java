package test.david.holamundo2021c1d.controlador;

import android.content.ContentValues;
import android.content.Context;

import test.david.holamundo2021c1d.modelo.MainDBContract;
import test.david.holamundo2021c1d.modelo.UsuariosModelo;

public class UsuariosControlador {

    // Comunicacion con la capa modelo
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context ctx){
        this.capaModelo = new UsuariosModelo(ctx);
    }

    public String crearUsuario(String nombreUsuario, String password, String password2){
        // Validar que las contraseñas coincidan
        if(!password.equals(password2)){
            // Detectamos que las contraseñas no coinciden
            return "ERR_NO_COINCICEN";
        }

        // Validar el largo de la contraseña
        if(password.length() < 8){
            return "ERR_LARGO_CARACTERES";
        }

        // En este punto las contraseñas coinciden
        this.capaModelo.crearUsuario(nombreUsuario, password);

        return "OK";
    }

    public boolean procesarInicioSesion(String nombre, String password){
        // Obtener los datos desde BD
        ContentValues usuario_bd = this.capaModelo.objeterUsuarioPorNombre(nombre);

        if(usuario_bd == null){
            // El usuario no existe
            return false;
        }

        // Validar las contraseñas
        String password_bd = usuario_bd.getAsString(MainDBContract.TablaUsuarios.COLUMNA_PASSWORD);
        if(password.equals(password_bd)){
            return true;
        }

        return false;
    }
}
